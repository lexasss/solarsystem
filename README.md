solarsystem
===========
The Solar system simulator.
The project is a fork of [SpaceSim](https://github.com/variostudio/spacesim) by variostudio

Requires

* Python 3.2+
* PyGame 1.9.2+

Usage:

    pythonXX main.py [-f filename.ini]

Arguments:

    -f filename.ini      - system configuration file (default: main.ini)
    -m method_name       - computation method: "rk" - Runge-Kutta (default), "euler" - Euler
    -s simulation_speed  - applied to reduces changes in speed per cycle (default: 0.1)
    -c simulation_cycles - simulation cycles per frame (default: 5)

Example:

    python32.exe main.py -f basic.ini

Keys to control the simulator:

    LEFT_CLICK [object] 
                = Select the object. The initially selected object is the Sun, 
                  or the first object in the configuration file
    LEFT_CLICK + DRAG [empty] 
                = Create a planet with random mass
                
    [, ]        = Select the previous / next object
    t           = Keep the selected object in the center of the view
    c           = Toggle displaying connections from objects to the selected object
    l           = Toggle displaying objects' labels
    d           = Toggle displaying distances from objects to the selected object
    f           = Toggle full screen
    ENTER       = Create a comet
    LEFT, RIGHT, UP, DOWN
                = Pan the view
    p or SPACE  = Pause
    q or ESC    = Exit

NOTES: 

* A lot of .ini files are included
* A comet is creating every 5-10 minutes
* Objects may collide, splitting to pieces or joining into a single object
* Precision of simulation is adjustable by editing T value in flyobj.py
