def __line( p1, p2 ):
    a = (p1[1] - p2[1])
    b = (p2[0] - p1[0])
    c = (p1[0] * p2[1] - p2[0] * p1[1])
    return a, b, -c

def intersection( x1, y1, x2, y2, x3, y3, x4, y4 ):
    a = __line( (x1, y1), (x2, y2) )
    b = __line( (x3, y3), (x4, y4) )
    d  = a[0] * b[1] - a[1] * b[0]
    dx = a[2] * b[1] - a[1] * b[2]
    dy = a[0] * b[2] - a[2] * b[0]
    if d != 0:
        x = dx / d
        y = dy / d
        return x, y
    else:
        return False
