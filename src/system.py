import math, random, pygame #, multiprocessing

from src.point import Point
from src.spaceobj import SpaceObject
from src.comet import Comet
from src.utils import intersection

EDGE_OFFSET = 4

# class __Obj:
#     def __init__( self, x, y, m, r, s, ms, ic ):
#         self.x = x
#         self.y = y
#         self.m = m
#         self.r = r
#         self.s = s
#         self.ms = ms
#         self.ic = ic
#         self.dtso = 0
#         self.dts = 0
#         self.d = False
#         self.b = []

class System:

    def __init__( self, cfg ):
        self._cfg = cfg
        self._items = cfg.system
        self._offset = Point( 0.0, 0.0 )
        self._zoom = 1.0  # not used in the current version
        self._mainStar = self._items[0]

        for obj in self._items:
            if obj.name == 'Sun':
                self._mainStar = obj

        self._selectedObject = self._mainStar

    def __translate( self, obj ):
        return (obj.x - self._offset.x, obj.y - self._offset.y)

    def __intersect( self, pt1, pt2, rect ):
        (x1, y1) = pt1
        (x2, y2) = pt2
        line = (x1, y1, x2, y2)
        line_top = (rect.left, rect.top, rect.right, rect.top)
        line_left = (rect.left, rect.top, rect.left, rect.bottom)
        line_bottom = (rect.left, rect.bottom, rect.right, rect.bottom)
        line_right = (rect.right, rect.top, rect.right, rect.bottom)

        inth = (0, 0)
        if y2 > y1:
            inth = intersection( *line, *line_top )
        else:
            inth = intersection( *line, *line_bottom )

        intv = (0, 0)
        if x2 > x1:
            intv = intersection( *line, *line_left )
        else:
            intv = intersection( *line, *line_right )

        if intv is not False and (rect.top <= intv[1] and intv[1] <= rect.bottom):
            return intv
        elif inth is not False and (rect.left <= inth[0] and inth[0] <= rect.right):
            return inth
        else:
            return (0, 0)

#     def simulateSingle(obj1, items, outDist, lock):
#         planetLost = False
#         planetToEatBySun = None

#         for obj2 in items:
#             if obj1 == obj2:
#                 continue

#             dist = max(0.5, math.hypot(obj1.x - obj2.x, obj1.y - obj2.y))
#             if obj2.s:
#                 obj1.dtso = dist

#             if obj2.ms:
#                 obj1.dts = dist
#                 if dist > outDist:
#                     obj1.d = True
#                     planetLost = True

#             # Collision detection
#             if dist < (obj1.r + obj2.r):
#                 if !obj1.ms and !obj2.ms:
#                     pass
# #                    asteroids = obj1.collideWith(obj2)
# #                    for ast in asteroids:
# #                        ast.initSurface(self._cfg.spaceColor)
# #                        ast.canSplit = False
# #                        ast.inCollision = True
# #                    self._items.extend(asteroids)
#                 else:
#                     if !obj1.ms:
#                         planetToEatBySun = obj1
#                     elif obj2 != self._mainStar:
#                         planetToEatBySun = obj2
#                 planetLost = True

#             # Calculate impacts
#             if not obj1.ic and not obj1.ic:
#                 if (obj2.m / (dist**2.0)) > 0.001:
#                     obj1.b.append(obj2)

#         return (planetLost, planetToEatBySun)

    def simulate( self ):
        planetLost = False

        # timer = pygame.time
        # ds = timer.get_ticks()

        # lock = multiprocessing.Lock()
        # manager = multiprocessing.Manager()
        # objs = manager.list()
        # selObj = None
        # for obj in self._items:
        #     objs.append(__Obj(obj.x, obj.y, obj.mass, obj.radius, obj == self._selectedObject, obj == self._mainStar))

        # poolSize = 2 * multiprocessing.cpu_count()
        # count = len(objs)
        # idx = 0
        # while count > 0:
        #     processes = []
        #     c = min(poolSize, count)
        #     for i in range(0, c):
        #         p = multiprocessing.Process(target=simulateSingle, args=(objs[idx], objs, self._cfg.outDist, lock))
        #         idx += 1
        #         p.start()
        #         processes.append(p)

        #     for p in processes:
        #         p.join()

        #     count -= c

        # for obj in self._items:
        #     objs.append(__Obj(obj.x, obj.y, obj.mass, obj.radius, obj == self._selectedObject, obj == self._mainStar))

        for obj1 in self._items:
            for obj2 in self._items:
                if obj1.destroyed or obj2.destroyed:
                    continue
                if obj1 == obj2:
                    continue

                dist = obj1.dist( obj2 )
                if obj2 == self._selectedObject:
                    obj1.distToSelected = dist

                # Out-of-the-system detection
                if obj2 == self._mainStar:
                    obj1.distToSun = dist
                    if dist > self._cfg.outDist:
                        obj1.destroyed = True
                        planetLost = True

                # Collision detection
                if dist < (obj1.radius + obj2.radius):
                    if obj1 != self._mainStar and obj2 != self._mainStar:
                        asteroids = obj1.collideWith( obj2 )
                        for ast in asteroids:
                            ast.initSurface( self._cfg.spaceColor )
                            ast.canSplit = False
                            ast.inCollision = True
                        self._items.extend( asteroids )
                    else:
                        if obj1 != self._mainStar:
                            self._mainStar.eat( obj1 )
                        elif obj2 != self._mainStar:
                            self._mainStar.eat( obj2 )
                    planetLost = True

                # Calculate impacts
                if not obj1.inCollision and not obj1.inCollision:
                    if (obj2.mass / (dist**2.0)) > 0.001:
                        obj1.bind(obj2)

        # d1 = pygame.time.get_ticks() - ds
        # ds = pygame.time.get_ticks()

        if planetLost:
            items = []
            for obj in self._items:
                if not obj.destroyed:
                    items.append( obj )
                else:
                    if obj == self._selectedObject:
                        self._selectedObject = None
            self._items = items

        # d2 = pygame.time.get_ticks() - ds
        # ds = pygame.time.get_ticks()

        for obj in self._items:
            obj.update( self._cfg.method, self._cfg.speed )
            if obj.name.find( 'Comet_' ) > -1:
                obj.updateTail( self._mainStar.pos )

        # d3 = pygame.time.get_ticks() - ds
        # return [d1, d2, d3]

    def drawSystem( self, screen, font, monofont ):
        rowHeight = 25
        index = 2

        showConn = self._cfg.showConnections

        if self._cfg.showDistances:
            infoTitle = monofont.render(
                '{0:<15} {1:<6} {2:<5}'.format( 'Name', 'Mass', 'Dist' ),
                True,
                pygame.Color( 'white' )
            )
            screen.blit( infoTitle, (10, rowHeight * index) )
            index += 1

        self._items.sort( reverse = True, key = lambda item: item.mass )

        for obj in self._items:
            visible = obj.isVisible( screen, self._offset, self._zoom )
            if visible:
                obj.draw( screen, self._offset, self._zoom )
            (px, py) = self.__translate( obj )
            if obj != self._selectedObject and self._selectedObject is not None:
                index += 1
                (sx, sy) = self.__translate( self._selectedObject )
                if self._cfg.showDistances and rowHeight*index < screen.get_height():
                    distLabel = monofont.render( '{0:<15} {1:<6.1f} {2:<5.0f}'.format(
                        obj.name, obj.mass, obj.distToSelected), True, obj.color )
                    screen.blit( distLabel, (10, rowHeight * index) )
                if showConn:
                    pygame.draw.line( screen, obj.color, (px, py), (sx, sy) )

            if obj == self._selectedObject and visible:
                pygame.draw.line( screen, obj.color, (px - obj.radius - 8, py), (px - obj.radius - 2, py) )
                pygame.draw.line( screen, obj.color, (px, py - obj.radius - 8), (px, py - obj.radius - 2) )
                pygame.draw.line( screen, obj.color, (px + obj.radius + 8, py), (px + obj.radius + 2, py) )
                pygame.draw.line( screen, obj.color, (px, py + obj.radius + 8), (px, py + obj.radius + 2) )
            if self._cfg.showLabels and (visible or showConn):
                nameLabel = font.render( obj.name, True, obj.color )

                offx = px - nameLabel.get_width() / 2
                offy = py - obj.radius - 3 - nameLabel.get_height()

                if not visible:
                    (offx, offy) = self.__intersect( (px, py), (sx, sy), screen.get_rect() )

                if offx < EDGE_OFFSET:
                    offx = EDGE_OFFSET
                elif (offx + nameLabel.get_width()) > (screen.get_width() - EDGE_OFFSET):
                    offx = screen.get_width() - nameLabel.get_width() - EDGE_OFFSET
                if offy < EDGE_OFFSET:
                    offy = EDGE_OFFSET
                elif (offy + nameLabel.get_height()) > (screen.get_height() - EDGE_OFFSET):
                    offy = screen.get_height() - nameLabel.get_height() - EDGE_OFFSET

                screen.blit( nameLabel, (int( offx ), int( offy )) )

        if self._selectedObject is not None:
            objLabel = monofont.render( '[%s]' % (self._selectedObject.name), True, self._selectedObject.color )
            screen.blit( objLabel, (10, 10) )

    def tryToSelect( self, x, y ):
        result = False
        for obj in self._items:
            (px, py) = self.__translate( obj )
            dist = math.sqrt( (px - x)**2 + (py - y)**2 )
            if dist < max( obj.radius, 12 ):
                self._selectedObject = obj
                result = True
                print( 'Selected %s' % obj.name )
                break
        return result

    def createPlanet( self, sx, sy, ex, ey ):
        name = SpaceObject.getNewName( 'Planet' )
        mass = random.randint( 10, 300 )

        r = random.randint( 128, 255 )
        g = random.randint( 128, 255 )
        b = random.randint( 128, 255 )
        color = pygame.Color( r, g, b )

        vx = float( ex - sx ) / 8.0
        vy = float( ey - sy ) / 8.0
        if self._cfg.trackSelectedObject:
            vx += self._mainStar.vx
            vy += self._mainStar.vy

        newPlanet = SpaceObject( name, mass, color, ex + self._offset.x, ey + self._offset.y, vx, vy )
        newPlanet.initSurface( self._cfg.spaceColor )
        self._items.append( newPlanet )

        print( newPlanet )

    def createComet( self ):
        comet = Comet.create( self._mainStar, self._cfg.cometDist, self._cfg.spaceColor )
        self._items.append( comet )

    def selectPrev( self ):
        if self._selectedObject is None:
            self._selectedObject = self._items[0]
        else:
            index = self._items.index( self._selectedObject ) - 1
            if index < 0:
                index = len( self._items ) - 1
            self._selectedObject = self._items[ index ]

    def selectNext( self ):
        if self._selectedObject is None:
            self._selectedObject = self._items[0]
        else:
            index = self._items.index( self._selectedObject ) + 1
            if index == len( self._items ):
                index = 0
            self._selectedObject = self._items[ index ]

    @property
    def offset( self ):
        return self._offset

    @offset.setter
    def offset( self, value ):
        self._offset = value

    @property
    def isInvalid( self ):
        return len( self._items ) < 2

    @property
    def selectedObject( self ):
        return self._selectedObject
