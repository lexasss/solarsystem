import math, random, sys, os, pygame

from src.point import Point
from src.config import Config
from src.system import System
from src.menu import Menu

class SolarSystem:

    def __init__( self ):

        self._screen = None
        self._bg = None
        self._cfg = None
        self._system = None
        self._fullScreenSize = (0, 0)

        # PyGame init
        pygame.init()
        pygame.display.set_caption( 'Solar Mechanics' )

        self._font = pygame.font.SysFont( 'Arial', 16 )
        self._monofont = pygame.font.SysFont( 'Consolas', 16 )

        try:
            self._cfg = Config()
            self._system = System( self._cfg )
        except Exception:
            print( Exception )
            sys.exit(1)

        self._screen = self.__createSurface( self._cfg )
        self._bg = self.__createBackground( self._cfg )

        self._menu = Menu( self._cfg )

        icon = pygame.image.load( os.path.join( 'img', 'icon.png' ) ).convert_alpha()
        pygame.display.set_icon( icon )

    def run( self ):
        done = False

        paused = False
        keyDownLeft = False
        keyDownRight = False
        keyDownUp = False
        keyDownDown = False

        mouseDownPos = Point( 0, 0 )

        timer = pygame.time.Clock()

        cfg = self._cfg

        while not done:
            timer.tick( 25 )

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    done = True
                    break
                elif event.type == pygame.KEYDOWN:
                    key = event.key
                    if key in (pygame.K_q, pygame.K_ESCAPE):
                        done = True
                        break
                    elif key in (pygame.K_p, pygame.K_SPACE):
                        paused = not paused
                    elif key == pygame.K_f:
                        cfg.toggleFullScreen()
                        self._screen = self.__createSurface( cfg )
                        self._bg = self.__createBackground( cfg )
                    elif key == pygame.K_c:
                        cfg.toggleShowConnections()
                    elif key == pygame.K_d:
                        cfg.toggleShowDistances()
                    elif key == pygame.K_t:
                        cfg.toggleTrackSelectedObject()
                    elif key == pygame.K_l:
                        cfg.toggleShowLabels()
                    elif key == pygame.K_RETURN:
                        self._system.createComet()
                    elif key == pygame.K_LEFTBRACKET:
                        self._system.selectPrev()
                    elif key == pygame.K_RIGHTBRACKET:
                        self._system.selectNext()
                    elif key == pygame.K_LEFT:
                        keyDownLeft = True
                    elif key == pygame.K_RIGHT:
                        keyDownRight = True
                    elif key == pygame.K_UP:
                        keyDownUp = True
                    elif key == pygame.K_DOWN:
                        keyDownDown = True

                elif event.type == pygame.KEYUP:
                    key = event.key
                    if key == pygame.K_LEFT:
                        keyDownLeft = False
                    elif key == pygame.K_RIGHT:
                        keyDownRight = False
                    elif key == pygame.K_UP:
                        keyDownUp = False
                    elif key == pygame.K_DOWN:
                        keyDownDown = False

                elif event.type == pygame.MOUSEBUTTONDOWN:
                    mouseDownPos = Point( event.pos[0], event.pos[1] )

                elif event.type == pygame.MOUSEBUTTONUP:
                    (x, y) = event.pos
                    isSelection = self._system.tryToSelect( mouseDownPos.x, mouseDownPos.y )
                    if not isSelection:
                        dist = math.sqrt( (x - mouseDownPos.x)**2 + (y - mouseDownPos.y)**2 )
                        if dist > 8:
                            self._system.createPlanet( mouseDownPos.x, mouseDownPos.y, x, y )
                        else:
                            self._menu.activateFocused()

            if paused:
                continue

            if not cfg.trackSelectedObject:
                x, y = self._system.offset
                step = 10
                if keyDownLeft:
                    x -= step
                if keyDownRight:
                    x += step
                if keyDownUp:
                    y -= step
                if keyDownDown:
                    y += step
                self._system.offset = Point( x, y )

            if random.randint( 0, 10000 ) == 1000:
                self._system.createComet()

            # ds = pygame.time.get_ticks()
            # d = [0.0, 0.0, 0.0]

            for step in range( cfg.cycles ):
                self._system.simulate()
                # dd = self._system.simulate()
                # i = 0
                # for dv in dd:
                #     d[i] += dv
                #     i += 1

            # d.append( pygame.time.get_ticks() - ds )
            # ds = pygame.time.get_ticks()

            selObj = self._system.selectedObject
            if cfg.trackSelectedObject and selObj is not None:
                self._system.offset = Point(
                    selObj.x - self._screen.get_width() / 2.0,
                    selObj.y - self._screen.get_height() / 2.0
                )

            # Put space to screen
            self._screen.blit( self._bg, (0, 0) )

            # Put each object to screen
            self._system.drawSystem( self._screen, self._font, self._monofont )

            # handle menu
            self._menu.draw( self._screen, cfg )
            (mx, my) = pygame.mouse.get_pos()
            self._menu.map( mx, my, cfg )

            #################
            # d.append( pygame.time.get_ticks() - ds )
            # self.__drawDebug( d )

            # Update screen
            pygame.display.update()

            if self._system.isInvalid:
                done = True
                print( 'Lost all planets' )
                break

        # Farewell
        print( 'Bye...' )

    # Static
    def __createSurface( self, cfg ):
        screenSize = cfg.display
        flags = 0

        if cfg.fullScreen:
            screenSize = self._fullScreenSize
            flags = flags | pygame.FULLSCREEN | pygame.NOFRAME | pygame.HWSURFACE

        screen = pygame.display.set_mode( screenSize, flags )
        if cfg.fullScreen and self._fullScreenSize[0] == 0:
            self._fullScreenSize = (screen.get_width(), screen.get_height())

        return screen

    def __createBackground( self, cfg ):
        # Space init
        bg = pygame.Surface( (self._screen.get_width(), self._screen.get_height()) )
        bg.fill( pygame.Color( cfg.spaceColor ) )

        # Draw fixed stars
        for _ in range( cfg.starNumber ):
            pygame.draw.circle( bg, pygame.Color( random.sample( cfg.starColors, 1 )[0] ), (
                                random.randrange( self._screen.get_width() ),
                                random.randrange( self._screen.get_height() )
                                ), 0
                              )

        return bg

    def __drawDebug( self, durations ):
        color = pygame.Color( 'white' )
        index = 0
        for d in durations:
            label = self._font.render( '{0:.0f}'.format(d), True, color )
            self._screen.blit( label, (self._screen.get_width() - 60, index * 20) )
            index += 1
