import collections

# Point structure (immutable)
Point = collections.namedtuple( 'Point', ['x', 'y'] )
