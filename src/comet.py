import math, random, pygame

from src.spaceobj import SpaceObject

# @class Comet
#   Represents a comet. Comet is a descendant of the SpaceObject class
class Comet( SpaceObject ):

    cometIndex = 0

    # @construction
    # @params
    #   name        - comet name
    #   mass        - comet mass
    #   color       - comet color (string)
    #   x, y        - comet location
    #   vx, vy      - comet speed
    def __init__( self, name, mass, color, x, y, vx, vy ):
        SpaceObject.__init__( self, name, mass, color, x, y, vx, vy )

        self._translate = 0.0
        self._tailLength = 1.0

    # @function updateTail
    #   updates the tail parameters
    # @params
    #   sunPos      - the location of sun {x, y}
    def updateTail( self, sunPos ):
        dx = self.x - sunPos.x
        dy = self.y - sunPos.y
        self._tailLength = min( 200, 5000.0/self.distToSun )
        self._translate = math.atan2( dy, dx )

    # @function draws
    #   draws the comet to screen
    # @params
    #   screen      - the surface to draw onto
    #   offset      - the viewport offset
    def draw( self, screen, offset, zoom ):
        SpaceObject.draw( self, screen, offset, zoom )

        if self._tailLength >= 1.0:
            a1 = self._translate + (0.05 + 2.0 / self._tailLength)
            a2 = self._translate - (0.05 + 2.0 / self._tailLength)
            tex1 = self.x + self._tailLength * math.cos( a1 ) - offset.x
            tey1 = self.y + self._tailLength * math.sin( a1 ) - offset.y
            tex2 = self.x + self._tailLength * math.cos( a2 ) - offset.x
            tey2 = self.y + self._tailLength * math.sin( a2 ) - offset.y

            pygame.draw.polygon( screen, self.color, [
                (self.x - offset.x, self.y - offset.y),
                (tex1, tey1),
                (tex2, tey2)
            ] )

    # Static methods

    # @function create
    #   creates and returns a new comet
    # @params
    #   sun         - the Sun or main star object
    #   maxDist     - maximum distance at which the comet will be created
    #                 (the minimum is maxDist / 2)
    #   spaceColor  - space color (string)
    # @return
    #   the created comet
    @staticmethod
    def create( sun, maxDist, spaceColor ):
        Comet.cometIndex += 1

        a = 2.0 * math.pi * random.random()
        dist = maxDist * (0.5 + 0.49 * random.random())
        x = dist * math.cos( a ) + sun.x
        y = dist * math.sin( a ) + sun.y

        initialSpeed = 0.15 * math.sqrt( sun.mass / dist )
        vx = initialSpeed * math.cos( a + math.pi / 2.0 ) + sun.vx
        vy = initialSpeed * math.sin( a + math.pi / 2.0 ) + sun.vy

        result = Comet(
            'Comet_{0}'.format( Comet.cometIndex ),
            1.0,
            pygame.Color( 'white' ),
            x, y, sun.vx + vx, sun.vy + vy
        )

        result.initSurface( spaceColor )
        print( result )

        return result
