import os, pygame

from src.point import Point

MENU_SLIDING_STEP = 0.1

class Images( dict ):

    def __init__( self, names ):
        dict.__init__({})
        for name in names:
            self[ name ] = pygame.image.load( os.path.join( 'img', name + '.png' ) ).convert_alpha()

    def __missing__( self, key ):
        return None

    def getFlag( self, enabled ):
        if enabled:
            return self[ 'enabled' ]
        else:
            return self[ 'disabled' ]

class MenuZone:

    def __init__( self, level, isFlag, offset, action ):
        self._level = level
        self._isFlag = isFlag
        self._offset = offset
        self._action = action
        self._location = Point( 0, 0 )

    def setLocation( self, value ):
        self._location = value

    def getRect( self, img ):
        return pygame.Rect( self._location.x, self._location.y, img.get_width(), img.get_height() )

    @property
    def level( self ):
        return self._level

    @property
    def isFlag( self ):
        return self._isFlag

    @property
    def offset( self ):
        return self._offset

    @property
    def action( self ):
        return self._action

    @property
    def x( self ):
        return self._location.x

    @property
    def y( self ):
        return self._location.y


class Menu():

    def __init__( self, cfg ):

        self._font = pygame.font.SysFont( 'Bell Gothic Std, Arial', 18 )
        self._fontColor = pygame.Color( '#1F9852' )

        self._images = Images([ 'flags', 'enabled', 'disabled', 'pull' ])

        self._menuVisibility = 0.0
        self._menuVisibilityChange = 0
        self._focused = None

        offsY = 10
        step = 86

        flags = self._images[ 'flags' ]
        pull = self._images[ 'pull' ]
        self._zones = {}
        self._zones[ 'flags' ] = MenuZone( 0, False, Point( 0, 0 ), None )
        self._zones[ 'connections' ] = MenuZone( 1, True, Point( 0, offsY + 0*step ), cfg.toggleShowConnections )
        self._zones[ 'distances' ] = MenuZone( 1, True, Point( 0, offsY + 1*step ), cfg.toggleShowDistances )
        self._zones[ 'tracking' ] = MenuZone( 1, True, Point( 0, offsY + 2*step ), cfg.toggleTrackSelectedObject )
        self._zones[ 'labels' ] = MenuZone( 1, True, Point( 0, offsY + 3*step ), cfg.toggleShowLabels )
        self._zones[ 'pull' ] = MenuZone( 1, False, Point(
            -flags.get_width() / 2,
            (flags.get_height() - pull.get_height()) / 2
            ), self.__openFlags )
        self._maxLevel = 1

    def __openFlags( self ):
        if self._menuVisibility == 0.0:
            self._menuVisibilityChange = 1

    def activateFocused( self ):
        if self._focused is not None:
            action = self._focused.action
            if action is not None:
                action()

    def map( self, x, y, cfg ):
        self._focused = None
        highestLevel = -1

        for (name, zone) in self._zones.items():
            img = None
            if zone.isFlag:
                img = self._images.getFlag( cfg.getFlag( name ) )
            else:
                img = self._images[ name ]
            if zone.level > highestLevel and zone.getRect( img ).collidepoint( x, y ):
                self._focused = zone
                highestLevel = zone.level

        if self._focused is None and self._menuVisibility > 0.0:    # outside, but the menu is visible
            self._menuVisibilityChange = -1

        if self._menuVisibilityChange < 0:
            self._menuVisibility = max( 0.0, self._menuVisibility - MENU_SLIDING_STEP )
            if self._menuVisibility == 0.0:
                self._menuVisibilityChange = 0
        elif self._menuVisibilityChange > 0:
            self._menuVisibility = min( 1.0, self._menuVisibility + MENU_SLIDING_STEP )
            if self._menuVisibility == 1.0:
                self._menuVisibilityChange = 0

    def draw( self, screen, cfg ):

        flags = self._images[ 'flags' ]
        x = screen.get_width() - (self._menuVisibility - 0.5) * flags.get_width()
        y = (screen.get_height() - flags.get_height()) / 2

        level = 0
        while level <= self._maxLevel:
            for (name, zone) in self._zones.items():
                if zone.level != level:
                    continue

                offset = zone.offset
                img = None
                label = None
                if zone.isFlag:
                    img = self._images.getFlag( cfg.getFlag( name ) )
                    label = self._font.render( name.title(), True, self._fontColor )
                else:
                    img = self._images[ name ]
                if img is not None:
                    zone.setLocation( Point( x + offset.x - img.get_width() / 2, y + offset.y ) )
                    screen.blit( img, (zone.x, zone.y) )
                if label is not None:
                    screen.blit( label, (x + offset.x - label.get_width() / 2, zone.y + img.get_height() - 5) )

            level += 1
