import math, random, pygame

from src.point import Point

# Object density
DENSITY = 0.3125

# New planet index

class SpaceObject:
    distToSun = 0.0
    distToSelected = 0.0
    destroyed = False
    canSplit = True
    inCollision = False

    planetIndex = 0

    # Creates new flying object like planet or star
    def __init__( self, name, mass, color, x, y, vx, vy ):
        self._name = name
        self._mass = mass
        self._color = color
        self._x = x
        self._y = y
        self._vx = vx
        self._vy = vy
        self._others = []

        self._spaceColor = pygame.Color( 'white' )
        self._image = None

        # self._maxDist = 0
        # self._minDist = 10000000

        self.updateRadius()

    def __str__( self ):
        return '{0}, ({1:.0f}, {2:.0f}) v=({3:.1f}, {4:.1f}), mass={5:.1f}, R={6:.1f}'.format(
            self._name, self._x, self._y, self._vx, self._vy, self._mass, self._radius )

    def initSurface( self, spaceColor ):
        if spaceColor is not None:
            self._spaceColor = spaceColor

        self.updateRadius()
        self._image = pygame.Surface( (2.0 * self._radius, 2.0 * self._radius) )
        self._image.fill( pygame.Color( self._spaceColor ) )
        pygame.draw.circle( self._image, self._color, (
            math.floor( self._radius + 0.5 ),
            math.floor( self._radius + 0.5 )
            ), math.floor( self._radius + 0.5 ) )

        # Funny drawing :)
        if self._name == 'Sun':
            pygame.draw.circle( self._image, pygame.Color( self._spaceColor ), (
                math.floor( 0.6*self._radius + 0.5 ),
                math.floor( 0.6*self._radius + 0.5 )
                ), 2 )
            pygame.draw.circle( self._image, pygame.Color( self._spaceColor ), (
                math.floor( 1.4*self._radius + 0.5 ),
                math.floor( 0.6*self._radius + 0.5 )
                ), 2 )
            pygame.draw.arc( self._image, pygame.Color( 'red' ), pygame.Rect(
                0.5 * self._radius, 0.8 * self._radius, self._radius, self._radius
                ), 5.0 * math.pi / 4.0, 7.0 * math.pi / 4.0, 2 )

    def updateRadius( self ):
        self._radius = max( 0.5, math.pow( self._mass / (4.0 / 3.0 * math.pi * DENSITY), 1.0 / 3.0 ) )

    def dist( self, other ):
        return max( 0.5, math.hypot( self._x - other.x, self._y - other.y ) )

    def bind( self, other ):
        self._others.append(other)

    def isVisible( self, screen, offset, zoom ):
        width = zoom * screen.get_width()
        height = zoom * screen.get_height()
        return (self._x + self._radius - offset.x >= 0.0 and
                self._x - self._radius - offset.x < width and
                self._y + self._radius - offset.y >= 0.0 and
                self._y - self._radius - offset.y < height)

    def draw( self, screen, offset, zoom ):
        screen.blit( self._image, (
            int( zoom * (self._x - self._radius - offset.x) ),
            int( zoom * (self._y - self._radius - offset.y) )
        ) )

    def split( self, other ):
        result = []

        if self._mass > 5.0:
            m1 = 0.2 + 0.8 * random.random()
            m2 = 1.0 - m1
            v = math.sqrt( self._vx**2 + self._vy**2 )
            a = math.atan2( self._vy, self._vx )
            ast1 = self.__createAsteroid( v, a + 0.2 * random.random(), m1, other )
            ast2 = self.__createAsteroid( v, a - 0.2 * random.random(), m2, other )
            result.extend( ast1 )
            result.extend( ast2 )
        else:
            result.append( self )

        return result

    def collideWith( self, other ):
        result = []

        if self._mass / other.mass < 3.0 and other.mass / self._mass < 3.0:
            if self.canSplit and other.canSplit:
                result.extend( self.__collide( other ) )
            self.inCollision = True
            other.inCollision = True
        elif self._mass > other.mass:
            if other.canSplit:
                self.eat( other )
        else:
            if self.canSplit:
                other.eat( self )

        return result

    def eat( self, other ):
        self._vx = (self._mass * self._vx + other.mass * other.vx) / (self._mass + other.mass)
        self._vy = (self._mass * self._vy + other.mass * other.vy) / (self._mass + other.mass)
        self._mass += other.mass
        self.initSurface( None )
        other.destroyed = True

    def update( self, method, speed ):
        if method == 'euler':
            self.__Eiler( speed )
        elif method == 'rk':
            self.__RungeKutta( speed )

        if not self.inCollision:
            self.canSplit = True

        self.inCollision = False

        self._others = []

    # Private methods

    # EILER version
    def __Eiler( self, speed ):
        ax, ay = 0.0, 0.0
        for other in self._others:
            if not other.destroyed:
                r = self.dist( other )
                ax += other.mass * (other.x - self._x) / r**3
                ay += other.mass * (other.y - self._y) / r**3

        self._vx += speed * ax
        self._vy += speed * ay

        self._x += speed * self._vx
        self._y += speed * self._vy

    # RUNGE-KUTTA method
    def __RungeKutta( self, speed ):
        self.__calcX( speed )
        self.__calcY( speed )

    def __fx( self, localX ):
        a = 0.0
        for other in self._others:
            if not other.destroyed:
                r = max( 0.5, math.hypot( other.x - localX, other.y - self._y ) )
                a += other.mass * (other.x - localX) / r**3.0

        return a

    def __fy( self, localY ):
        a = 0.0
        for other in self._others:
            if not other.destroyed:
                r = max( 0.5, math.hypot( other.x - self._x, other.y - localY ) )
                a += other.mass * (other.y - localY) / r**3.0

        return a

    def __calcX( self, t ):
        fx = self.__fx
        x = self._x
        vx = self._vx

        k1 = t * fx(x)
        q1 = t * vx

        k2 = t * fx(x + q1 / 2.0)
        q2 = t * (vx + k1 / 2.0)

        k3 = t * fx(x + q2 / 2.0)
        q3 = t * (vx + k2 / 2.0)

        k4 = t * fx(x + q3)
        q4 = t * (vx + k3)

        self._vx += (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0
        self._x += (q1 + 2.0 * q2 + 2.0 * q3 + q4) / 6.0

    def __calcY( self, t ):
        fy = self.__fy
        y = self._y
        vy = self._vy

        k1 = t * fy(y)
        q1 = t * vy

        k2 = t * fy(y + q1 / 2.0)
        q2 = t * (vy + k1 / 2.0)

        k3 = t * fy(y + q2 / 2.0)
        q3 = t * (vy + k2 / 2.0)

        k4 = t * fy(y + q3)
        q4 = t * (vy + k3)

        self._vy += (k1 + 2.0 * k2 + 2.0 * k3 + k4) / 6.0
        self._y += (q1 + 2.0 * q2 + 2.0 * q3 + q4) / 6.0

    def __collide( self, other ):
        result = []

        v1 = math.sqrt( self._vx**2 + self._vy**2 )
        a1 = math.atan2( self._vy, self._vx )
        v2 = math.sqrt( other.vx**2 + other.vy**2 )
        a2 = math.atan2( other.vy, other.vx )

        if math.fabs( v1 - v2 ) < 2.0 and (math.fabs( a1 - a2 ) < 0.3 or math.fabs( a1 - a2 ) > (2.0 * math.pi - 0.3)):
            result.append( self.__join( other, (v1 * self._mass + v2 * other.mass) / (self._mass + other.mass), (
                a1 * self._mass + a2 * other.mass) / (self._mass + other.mass) ) )
        else:
            asteroids = self.split( other )
            result.extend( asteroids )

            asteroids = other.split( self )
            result.extend( asteroids )

        self.destroyed = True
        other.destroyed = True

        return result

    def __join( self, other, v, a ):
        vx = v * math.cos(a)
        vy = v * math.sin(a)

        return SpaceObject(
            SpaceObject.getNewName( self._name, other.name ),
            self._mass + other.mass,
            self._color,
            self._x,
            self._y,
            vx, vy
        )

    def __createAsteroid( self, v, a, m, other ):
        result = []
        vx = v * math.cos(a)
        vy = v * math.sin(a)
        m = m * self._mass
        if m >= 1.0:
            ast = SpaceObject( SpaceObject.getNewName( self._name ), m, self._color, self._x, self._y, vx, vy )
            if ast.mass > max( self._mass / 3.0, 5.0 ):
                result.extend( ast.split( other ) )
            else:
                result.append( ast )
        return result

    # Static methods

    @staticmethod
    def getNewName( *names ):
        SpaceObject.planetIndex += 1

        result = ''
        for name in names:
            parentName = name.split( '_' )[0]
            if parentName != 'Planet':
                result += '{0}_'.format( parentName )

        if not result:
            result = 'Planet_'

        result += '{0}'.format( SpaceObject.planetIndex )
        return result

    # Properties

    @property
    def name( self ):
        return self._name

    @property
    def mass( self ):
        return self._mass

    @property
    def radius( self ):
        return self._radius

    @property
    def color( self ):
        return self._color

    @property
    def x( self ):
        return self._x

    @property
    def y( self ):
        return self._y

    @property
    def vx( self ):
        return self._vx

    @property
    def vy( self ):
        return self._vy

    @property
    def pos( self ):
        return Point( self._x, self._y )
