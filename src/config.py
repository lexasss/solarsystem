import configparser, argparse, textwrap, pygame

from src.spaceobj import SpaceObject

class Config:

    def __init__( self ):
        self._fullScreen = True
        self._showConnections = False
        self._showDistances = True
        self._trackSelectedObject = False
        self._showLabels = False

        parser = argparse.ArgumentParser(
            description = 'Solar mechanics simulator',
            formatter_class = argparse.RawDescriptionHelpFormatter,
            epilog = textwrap.dedent( '''\
                Game controls:
                    LEFT CLICK [object] 
                                = Select the object. The initially selected object is the Sun, 
                                  or the first object in the configuration file
                    LEFT CLICK + DRAG [empty] 
                                = Create a planet with random mass
                    t           = Keep the selected object in the center of the view
                    c           = Toggle displaying object-selected_object lines
                    l           = Toggle displaying objects' labels
                    d           = Toggle displaying object-selected_object distances
                    f           = Toggle full screen
                    ENTER       = Create a comet
                    LEFT, RIGHT, UP, DOWN
                                = Pan the view
                    [, ]        = Select the previous / next object
                    p or SPACE  = Pause
                    q or ESC    = Exit
            ''')
        )

        parser.add_argument( '-f', '--file',
                             dest = 'file',
                             default = 'presets/main.ini',
                             help = 'configuration file (default: presets/main.ini)'
                           )

        parser.add_argument( '-m', '--method',
                             dest = 'method',
                             default = 'rk',
                             choices = ['rk', 'euler'],
                             help = 'computation method: "rk" - Runge-Kutta (default), "euler" - Euler'
                           )

        parser.add_argument( '-s', '--speed',
                             dest = 'speed',
                             type = float,
                             default = '0.1',
                             help = 'multiplyer to reduces changes in speed per cycle (default: 0.1)'
                           )

        parser.add_argument( '-c', '--cycles',
                             dest = 'cycles',
                             type = int,
                             default = '5',
                             choices = range(1, 21),
                             help = 'computation cycles per frame (default: 5)'
                           )

        args = parser.parse_args()

        self._method = args.method
        self._speed = args.speed
        self._cycles = args.cycles

        self._config = configparser.ConfigParser()
        self._config.read( args.file )

        systemParams = self._config[ 'System' ]

        width = int( systemParams.get( 'WIN_WIDTH', 800 ) )
        height = int( systemParams.get( 'WIN_HEIGHT', 640 ) )
        self._stars = int( systemParams.get( 'STAR_NUM', 100 ) )
        self._cometDist = int( systemParams.get( 'COMET_DIST', 5000 ) )
        self._outDist = int( systemParams.get( 'OUT_DIST', 10000 ) )
        self._fullScreen = bool( systemParams.get( 'FULL_SCREEN', self._fullScreen ) )
        self._showConnections = bool( systemParams.get( 'SHOW_CONN', self._showConnections ) )
        self._showDistances = bool( systemParams.get( 'SHOW_DIST', self._showDistances ) )
        self._trackSelectedObject = bool( systemParams.get( 'TRACK_OBJ', self._trackSelectedObject ) )
        self._showLabels = bool( systemParams.get( 'SHOW_LABELS', self._showLabels ) )

        self._display = (width, height)

        colors = systemParams.get( 'STAR_COLORS', '#6080FF,#FFA0A0,#EEEEEE,#C060FF,#60C0FF' )
        self._starColors = colors.split( ',' )

        self._spaceColor = systemParams.get( 'SPACE_COLOR', '#000022' )

        print( 'Simulation parameters:' )
        print( '  method: {0}'.format( self._method ) )
        print( '  speed : {0}'.format( self._speed ) )
        print( '  cycles: {0}'.format( self._cycles ) )

    def getFlag( self, name ):
        value = False

        if name == 'connections':
            value = self._showConnections
        elif name == 'distances':
            value = self._showDistances
        elif name == 'tracking':
            value = self._trackSelectedObject
        elif name == 'labels':
            value = self._showLabels
        elif name == 'fullscreen':
            value = self._fullScreen

        return value

    def toggleShowConnections( self ):
        self._showConnections = not self._showConnections

    def toggleShowDistances( self ):
        self._showDistances = not self._showDistances

    def toggleTrackSelectedObject( self ):
        self._trackSelectedObject = not self._trackSelectedObject

    def toggleShowLabels( self ):
        self._showLabels = not self._showLabels

    def toggleFullScreen( self ):
        self._fullScreen = not self._fullScreen

    @property
    def showConnections( self ):
        return self._showConnections

    @property
    def showDistances( self ):
        return self._showDistances

    @property
    def trackSelectedObject( self ):
        return self._trackSelectedObject

    @property
    def showLabels( self ):
        return self._showLabels

    @property
    def fullScreen( self ):
        return self._fullScreen

    @property
    def method( self ):
        return self._method

    @property
    def speed( self ):
        return self._speed

    @property
    def cycles( self ):
        return self._cycles

    @property
    def display( self ):
        return self._display

    @property
    def starNumber( self ):
        return self._stars

    @property
    def starColors( self ):
        return self._starColors

    @property
    def spaceColor( self ):
        return self._spaceColor

    @property
    def cometDist( self ):
        return self._cometDist

    @property
    def outDist( self ):
        return self._outDist

    @property
    def system( self ):
        result = []

        for i in self._config.sections():
            if i != 'System':
                objParams = self._config[i]
                obj = SpaceObject( i,
                                   float( objParams.get( 'Mass', 10.0 ) ),
                                   pygame.Color( objParams.get( 'color', 'white' ) ),
                                   float( objParams.get( 'X', 100.0 ) ),
                                   float( objParams.get( 'Y', 100.0 ) ),
                                   float( objParams.get( 'VX', 0.1 ) ),
                                   float( objParams.get( 'VY', 1.0 ) )
                                 )

                obj.initSurface( self._spaceColor )

                print( obj )
                result.append( obj )

        if len(result) == 0:
            raise Exception( 'The configuration file has no objects defined' )

        return result
